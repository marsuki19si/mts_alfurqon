<?php

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Auth_model', 'auth_model');
		$this->load->model('Calon_model', 'calon_model');
	}
	public function index()
	{
		if ($this->session->userdata('id_pengguna')) {
			redirect('Admin/Home');
		}

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == false) {
			$this->load->view('vw_login');
		} else {
			$this->login();
		}
	}

	public function login()
	{
		$this->load->model('auth_model');
		$this->load->library('form_validation');

		$rules = $this->auth_model->rules();
		$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() == FALSE) {
			return $this->load->view('vw_login');
		}

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level = $this->input->post('level');
		if ($level == 'admin') {
			if ($this->auth_model->login($username, $password)) {
				redirect('Admin/Home');
			} else {
				$this->session->set_flashdata('message', 'Login Gagal, pastikan username dan passwrod benar!');
			}
		} elseif ($level == 'calonsiswa') {
			if ($this->calon_model->login($username, $password)) {
				redirect('CalonSiswa/Home');
				// dd($username);
			} else {
				$this->session->set_flashdata('message', 'Login Gagal, pastikan username dan passwrod benar!');
			}
		}elseif ($level == 'bendahara') {
			if ($this->auth_model->login($username, $password)) {
				redirect('Bendahara/Home');
			} else {
				$this->session->set_flashdata('message', 'Login Gagal, pastikan username dan passwrod benar!');
			}
		}elseif ($level == 'panitia') {
			if ($this->auth_model->login($username, $password)) {
				redirect('PanitiaPSB/Home');
			} else {
				$this->session->set_flashdata('message', 'Login Gagal, pastikan username dan passwrod benar!');
			}
		}

		$this->load->view('vw_login');
	}

	// public function login()
	// {
	// 	$username = $this->input->post('username');
	// 	$password = $this->input->post('password');

	// 	$user = $this->auth_model->read_by_username($username);

	// 	if ($user) {

	// 		if (password_verify($password, $user->password)) {

	// 			$session = [
	// 				'id_pengguna' => $user->id_pengguna,
	// 				'nama_pengguna' => $user->nama_pengguna,
	// 				'username' => $user->username,
	// 				'hp_pengguna' => $user->hp_pengguna
	// 			];
	// 			$this->session->set_userdata($session);
	// 			redirect('Admin/Home');

	// 		} else {
	// 			$this->session->set_flashdata('error', 'Wrong password!');
	// 			redirect('');

	// 		}
	// 	} else {
	// 		$this->session->set_flashdata('error', 'Wrong username!');
	// 		redirect('');
	// 		var_dump($user);
	// 		die();
	// 	}
	// }

	public function logout()
	{
		$this->load->model('auth_model');
		$this->auth_model->logout();
		redirect('Landing');
	}

	public function regis()
	{
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required', [
			'required' => 'Nama Pengguna Wajib di isi'
		]);
		$this->form_validation->set_rules('no_hp', 'No Hp', 'required', [
			'required' => 'No Hp Pengguna Wajib di isi'
		]);
		$this->form_validation->set_rules('username', 'username', 'required', [
			'required' => 'username Pengguna Wajib di isi'
		]);
		$this->form_validation->set_rules(
			'password',
			'Password',
			'required|trim|min_length[5]',
			[
				'min_length' => 'Password Terlalu Pendek',
				'required' => 'password Pengguna Wajib di isi'
			]
		);
		if ($this->form_validation->run() == false) {
			$this->load->view('vw_registrasi');
		} else {
			$data = [
				'nama_lengkap' => htmlspecialchars($this->input->post('nama_lengkap', true)),
				'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
				'username' => htmlspecialchars($this->input->post('username', true)),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
			];
			$this->calon_model->insert($data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Registrasi Berhasil</div>');
			redirect('Auth/login');
		}
	}

}