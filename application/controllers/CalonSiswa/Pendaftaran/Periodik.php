<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Periodik extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this -> load -> library('form_validation');
    }
    public function index()
    {

        $this->load->view('calonsiswa/header');
        $this->load->view('calonsiswa/Pendaftaran/vw_periodik');
        $this->load->view('calonsiswa/footer');

    }


}