<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CalonSiswa extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('PendaftaranCalonSiswa_Model','calonsiswa');
		$this->load->library('form_validation');
	}
	

    public function index()
    {

        $this->load->view('calonsiswa/header');
        $this->load->view('calonsiswa/Pendaftaran/vw_calonsiswa');
        $this->load->view('calonsiswa/footer');

    }

    public function add()
	{
		$this->form_validation->set_rules('nisn', 'NISN', 'required', [
			'required' => 'NISN Pengguna Wajib di isi'
		]);
		if($this->form_validation->run() == false){
			$this->load->view('vw_calonsiswa');
		}else {
			$data = [
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'nisn' => $this->input->post('nisn'),
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'tgl_lahir' => $this->input->post('tanggal_lahir'),
				'alamat' => $this->input->post('alamat'),
				'tempat_tinggal' => $this->input->post('tempat_tinggal'),
				'transportasi' => $this->input->post('transportasi'),
				
			];
			$this->calonsiswa->insert($data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Pendaftaran Calon Siswa Berhasil Berhasil</div>');
			redirect('Calonsiswa');
		}	
	}	
    

}