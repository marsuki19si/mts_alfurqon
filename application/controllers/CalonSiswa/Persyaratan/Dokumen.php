<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dokumen extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this -> load -> library('form_validation');
    }
    public function index()
    {

        $this->load->view('calonsiswa/header');
        $this->load->view('calonsiswa/Persyaratan/vw_dokumen');
        $this->load->view('calonsiswa/footer');

    }


}