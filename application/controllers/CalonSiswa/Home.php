<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('calon_model');
        if (!$this->calon_model->current_user()) {
            redirect('auth/login');
        }
    }

    // ... ada kode lain di sini ...
    public function index()
    {

        $this->load->view('calonsiswa/header');
        $this->load->view('calonsiswa/index');
        $this->load->view('calonsiswa/footer');

    }

}