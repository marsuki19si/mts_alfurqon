<!-- Begin Page Content -->
<div class="container-fluid">
	<h1 class="h3 mb-4 text-gray-800"><?php echo $judul; ?></h1>
	<div class="row justify-content-center">
		<div class="col-md-12 ">
			<div class="card">
				<div class="card-header">
					Form Tambah Data Siswa
				</div>
				<div class="card-body">
					<form action="" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="id_siswa" value="<?= $siswa['id_siswa']; ?>">
						<div class="form-group">
							<label for="nama_lengkap">Nama UKM</label>
							<input name="nama_lengkap" value="<?= $siswa['nama_lengkap']; ?>" type="text" class="form-control" id="nama_lengkap" placeholder="Nama UKM">
							<?= form_error('nama_lengkap', '<small class="text-danger pl-3">', '</small>'); ?>
						</div>
						<div class="form-group">
							<label for="no_hp">NO HP</label>
							<input name="no_hp" value="<?= $siswa['no_hp']; ?>" type="text" class="form-control" id="no_hp" placeholder="NO HP">
							<?= form_error('no_hp', '<small class="text-danger pl-3">', '</small>'); ?>
						</div>
						<div class="form-group">
							<label for="username">Username</label>
							<input value="<?= $siswa['username']; ?>" name="username" type="text" class="form-control" id="username" placeholder="Username">
							<?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
						</div>
						<div class="form-group">
							<label for="gambar">Gambar</label>
							<input name="gambar" type="file" value="<?= set_value('gambar'); ?>" class="form-control" id="gambar" placeholder="Gambar">
							<?= form_error('gambar', '<small class="text-danger pl-3">', '</small>'); ?>
						</div>
						
						<a href="<?= base_url('Admin/CalonSiswa') ?>" class="btn btn-danger">Tutup</a>
						<button type="submit" name="tambah" class="btn btn-primary float-right">Ubah Siswa</button>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
