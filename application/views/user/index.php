<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>PPDB | MTS Al-Furqon</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href=<?= base_url("assets/img/favicon.png") ?> rel="icon">
  <link href=<?= base_url("assets/img/User_V_i_P-Home.png") ?> rel="User_V_i_P-Home">

  <!-- Google Fonts -->
  <link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href=<?= base_url("assets/vendor/aos/aos.css") ?> rel="stylesheet">
  <link href=<?= base_url("assets/vendor/bootstrap/css/bootstrap.min.css") ?> rel="stylesheet">
  <link href=<?= base_url("assets/vendor/bootstrap-icons/bootstrap-icons.css") ?> rel="stylesheet">
  <link href=<?= base_url("assets/vendor/boxicons/css/boxicons.min.css") ?> rel="stylesheet">
  <link href=<?= base_url("assets/vendor/glightbox/css/glightbox.min.css") ?> rel="stylesheet">
  <link href=<?= base_url("assets/vendor/swiper/swiper-bundle.min.css") ?> rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href=<?= base_url("assets/css/style.css") ?> rel="stylesheet">
  <link href=<?= base_url("assets/css/style2.css") ?> rel="stylesheet">

  <!-- =======================================================
  * Template Name: BizLand - v3.9.1
  * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>


  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

      <h1 class="logo"><a href="index.html">MTS AL-FURQON<span></span></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo"><img src=<?= base_url("assets/img/logo.png") ?> alt=""></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Beranda</a></li>
          <li><a class="nav-link scrollto" href="#about">Tentang Kami</a></li>
          <li><a class="nav-link scrollto" href="#visimisi">Visi Misi</a></li>
          <li><a class="nav-link scrollto " href="#ekskul">Ekskul</a></li>
          <li><a class="nav-link scrollto" href="#fasilitas">Fasilitas</a></li>
          <li><a class="nav-link scrollto" href="#prestasi">Prestasi</a></li>
          <li><a class="nav-link scrollto" href="#contact">Kontak</a></li>
          <li><a class="nav-link scrollto" href="Auth">Login</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <h1>Welcome to <span>MTS Al-FURQON</span></h1>
      <h2 style="font-size:20px;text-align:justify">Website ini merupakan website resmi Pendaftaran Peserta Didik Baru
        MTS Al-Furqon Raja Bejamu. Untuk melakukan pendaftaran silahkan klik menu daftar atau jika sudah mendaftar
        silahkan cetak bukti pendaftarannya melalui menu print. Untuk informasi lebih lanjut bisa menghubungi Panitia
        PPDB melalui No.Tlp/HP berikut 081374665318</h2>
      <div class="d-flex">
        <a href="<?= base_url('Auth/regis/'); ?>" class="btn-get-started scrollto">Daftar</a>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">


    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Tentang Kami</h2>

        </div>

        <div class="row">
          <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100" style="margin-top:50px">
            <img src="assets/img/about.JPG" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up"
            data-aos-delay="100" style="text-align: justify;font-size:14px;">
            <h3>Sejarah</h3>
            <p>

              Diawali dengan melihat kondisi daerah Raja Bejamu dan sekitarnya yang pada tahun 2000an dalam kondisi yang
              terbelakang terutama dalam hal dunia pendidikan baik pendidikan umum ( SD SMP dan SMA ), apalagi
              pendidikan agama (MDA, MI MTs dan MA). Menyikapi kondisi tersebut, maka mulailah dirintis pendirian
              sekolah MDA (Madrasah Diniyah Awaliyah). </p>
            <p>
              Seiring perjalanan waktu MDA ini mulai berkembang dengan baik, beberapa tahun kemudian MDA ini mulai
              memiliki tanah dan gedung sendiri walau dalam bentuk yang sederhana. Dengan pertolongan dan rahmat dari
              Allah SWT lewat perjuangan para pendirinya maka MDA ini menjadi cikal bakal berdirinya yayasan Al-Furqon
              Raja Bejamu.</P>
            <p>Yayasan Al-Furqon Raja Bejamu berdiri pada tanggal 1 Juli 2006 dengan melaksanakan kegiatan program
              pendidikan selain MDA, juga membuka pendidikan anak usia dini (PAUD) dan pendidikan Madrasah Tsanawiyah
              (MTs). Al-Furqon Raja Bejamu </p>
            <P> Sejak saat itu pergantian pimpinan Madrasah sebagai berikut:
              <br>1. Zulfan menjabat dari tahun 2006 – 2008
              <br>2. Sugiati, S. Pd.I menjabat dari tahun 2008 – 2013
              <br>3. Arifin, S. Pd menjabat dari tahun 2013 – sekarang
            </P>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->



    <!-- ======= VisiMisi ======= -->
    <section id="visimisi" class="visimisi section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Visi Misi</h2>
          <h3>Visi</h3>
          <p class="visiMisi">Terwujudnya lembaga Pendidikan yang dapat mencetak generasi cerdas, kreatif dan
            berakhlakul karimah.</p>
          <h3>Misi</h3>
          <P class="visiMisi" style="margin-top:-10px;">
            <br>1. Membentuk generasi yag beriman, beramal dan berakhlakul karimah.
            <br>2. Mengembangkan serta memberdaayakan program-program pendidikan.
            <br>3. Menyiapkan lulusan yang memadai, dan pengajaran yang kreatif, inovatif dan transformatif
            <br>4. Menerapkan keilmuan yag bernuansa islami dan berwawasan iptek
          </P>

        </div>
      </div>
    </section><!-- End visimisi Section -->

    <!-- ======= ekskul Section ======= -->
    <section id="ekskul" class="ekskul">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Ekstrakurikuler</h2>
          <h3>Ekstrakurikuler <span>MTS Al-Alfurqon </span></h3>
        </div>

        <div class="row">
          
          <?php $i = 1; ?>
          <?php foreach ($ekskul as $us): ?>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
              <span class="border border-white">
              <div class="member">
                <div class="member-img">
                  <img src=" <?= base_url('uploads/') . $us['gambar']; ?>" class="img-fluid" alt="">
                </div>
                  <h4>
                    <?= $us['nama_ekskul']; ?>
                  </h4>
                  <p style="font-size:14px;text-align:justify">
                    <?= $us['deskripsi']; ?>
                  </p>
              </span>

                  
                </div>
              </div>
            <?php $i++; ?>
          <?php endforeach; ?>

          <!-- <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="member">
              <div class="member-img">
                <img src="assets/img/team/team-2.jpg" class="img-fluid" alt="">
              </div>
              <div class="member-info">
                <h4>Nasyid</h4>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
            <div class="member">
              <div class="member-img">
                <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
              </div>
              <div class="member-info">
                <h4>Seni Tari</h4>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
            <div class="member">
              <div class="member-img">
                <img src="assets/img/team/team-4.jpg" class="img-fluid" alt="">
              </div>
              <div class="member-info">
                <h4>xyz</h4>
              </div>
            </div>
          </div> -->

        </div>

      </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Team Section ======= -->
    <section id="fasilitas" class="fasilitas section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Fasilitas</h2>
          <h3>Fasilitas <span>MTS Al-Furqon</span></h3>
        </div>

<div class="row">

          <?php $i = 1; ?>
          <?php foreach ($fasilitas as $us): ?>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
               <span class="border border-white">
              <div class="member">
                <div class="member-img">
                  <img src=" <?= base_url('uploads/') . $us['gambar']; ?>" class="img-fluid" alt="">

                </div>
                <div class="member-info">
                  <h4>
                    <?= $us['nama_fasilitas']; ?>
                  </h4>
                   <p style="font-size:14px;text-align:justify">
                    <?= $us['deskripsi']; ?>
                  </p>
                  </span>
                  </div>
                </div>
              </div>
            <?php $i++; ?>
          <?php endforeach; ?>
                                      
                                       
                      
          <!-- <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="member">
              <div class="member-img">
                <img src="assets/img/team/team-1.jpg" class="img-fluid" alt="">

              </div>
              <div class="member-info">
                <h4>Ruang Kelas</h4>

              </div>
            </div>
          </div> -->

        </div>
      </div>
    </section><!-- End Team Section -->

    <!-- ======= Team Section ======= -->
    <section id="prestasi" class="prestasi section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Prestasi</h2>
          <h3>Prestasi <span>MTS Al-Furqon</span></h3>
        </div>

<div class="row">

          <?php $i = 1; ?>
          <?php foreach ($prestasi as $us): ?>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
              <div class="member">
                <span class="border border-white">
                <div class="member-img">
                  <img src=" <?= base_url('uploads/') . $us['gambar']; ?>" class="img-fluid" alt="">

                </div>
                <div class="member-info">
                  <h4>
                    <?= $us['nama_prestasi']; ?>
                  </h4>
                   <p style="font-size:14px;text-align:justify">
                    <?= $us['deskripsi']; ?>
                  </p>
                </span>
                  </div>
                </div>
              </div>
            <?php $i++; ?>
          <?php endforeach; ?>

        </div>

      </div>
    </section><!-- End Team Section -->


    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Kontak</h2>
          <h3><span>Kontak Kami</span></h3>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-6">
            <div class="info-box mb-4">
              <i class="bx bx-map"></i>
              <h3>Alamat</h3>
              <p>Jl. Poros Raja Bejamu,Kec.Sinaboi,Kab Rokan Hilir,Riau</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-envelope"></i>
              <h3>Email</h3>
              <p>alfurqon@gmail.com</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-phone-call"></i>
              <h3>Telephone</h3>
              <p>081374665318</p>
            </div>
          </div>

        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
        </div>

      </div>


  </main><!-- End #main -->

  <footer id="footer">


    <div class="container py-4">
      <div class="container px-4 px-lg-5">
        <div class="small text-center text-muted">Copyright &copy; PPDB Al-Furqon</div>

      </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
      class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src=<?= base_url("assets/vendor/purecounter/purecounter_vanilla.js") ?>></script>
  <script src=<?= base_url("assets/vendor/aos/aos.js") ?>></script>
  <script src=<?= base_url("assets/vendor/bootstrap/js/bootstrap.bundle.min.js") ?>></script>
  <script src=<?= base_url("assets/vendor/glightbox/js/glightbox.min.js") ?>></script>
  <script src=<?= base_url("assets/vendor/isotope-layout/isotope.pkgd.min.js") ?>></script>
  <script src=<?= base_url("assets/vendor/swiper/swiper-bundle.min.js") ?>></script>
  <script src=<?= base_url("assets/vendor/waypoints/noframework.waypoints.js") ?>></script>
  <script src=<?= base_url("assets/vendor/php-email-form/validate.js") ?>></script>

  <!-- Template Main JS File -->
  <script src=<?= base_url("assets/js/main.js") ?>></script>

</body>

</html>