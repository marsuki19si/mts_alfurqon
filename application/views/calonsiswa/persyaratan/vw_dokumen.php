<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Custom Theme Style -->
    <link href=<?= base_url("assets3/build/css/custom.min.css") ?> rel="stylesheet">
</head>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Formulir Persyaratan </h3>
                 
                <a href ="<?= base_url('index.php/download/proses_download'); ?>"> Cetak Surat Pernyataan</a> 
                
            </div>
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 ">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Dokumen</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <!-- Smart Wizard -->

                            <div id="wizard" class="form_wizard wizard_horizontal">

                                <div id="step-1">
                <form class="form-horizontal form-label-left">
                <div class="form-group">
                <label for="kk">Kartu keluarga <span class="text-danger">*</span></label>
                <input class="form-control" type="file" name="kk">
                <small class="form-text text-muted">Foto scan kartu keluarga. ukuran max 3MB. , format: JPG,JPEG,PNG</small>
                <?=form_error('kk', '<small class="form-text text-danger">', '</small>');?>
              </div>
                <div class="form-group">
                <label for="ijazah">Ijazah <span class="text-danger">*</span></label>
                <input class="form-control" type="file" name="ijazah">
                <small class="form-text text-muted">Foto scan SHUN atau Surat Keterangan Lulus dari SMP/MTs telah dilegalisir. Ukuran max 3MB, format: JPG,JPEG,PNG</small>
                <?=form_error('ijazah', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="akta_kelahiran">Akta Kelahiran <span class="text-danger">*</span></label>
                <input class="form-control" type="file" name="akta_kelahiran">
                <small class="form-text text-muted">Foto scan Akta Kelahiran. ukuran max 3MB. , format: JPG,JPEG,PNG</small>
                <?=form_error('akta_kelahiran', '<small class="form-text text-danger">', '</small>');?>
              </div>
               <div class="form-group">
                <label for="kip">Kartu Indonesia Pintar (KIP) <span class="text-danger"></span></label>
                <input class="form-control" type="file" name="kip">
                <small class="form-text text-muted">Foto scan KIP. ukuran max 1MB. , format: JPG,JPEG,PNG</small>
               <?= form_error('kip', '<small class="form-text text-danger">', '</small>'); ?>
               </div>
              <div class="form-group">
                <label for="surat_pernyataan">Surat Pernyataan <span class="text-danger">*</span></label>
                <input class="form-control" type="file" name="surat_pernyataan">
                <small class="form-text text-muted">Foto scan Surat Pernyataan. ukuran max 3MB. , format: JPG,JPEG,PNG</small>
                     <?=form_error('akta_kelahiran', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="pas_foto">Pas Foto 3x4 <span class="text-danger">*</span></label>
                <input class="form-control" type="file" name="pas_foto">
                <small class="form-text text-muted">Pas foto hitam putih ukuran 3x4. ukuran max 1MB. , format: JPG,JPEG,PNG</small>
                <?=form_error('pas_foto', '<small class="form-text text-danger">', '</small>');?>
              </div>
                                        <button type="submit" name="tambah"
                                            class="btn btn-primary float-right">Kirim</button>
                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>



</html>