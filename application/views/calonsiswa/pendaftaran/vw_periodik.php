
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Custom Theme Style -->
    <link href=<?=base_url("assets3/build/css/custom.min.css")?> rel="stylesheet">
</head>

           <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Formulir Pendaftaran </h3>
              </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Periodik Siswa</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <!-- Smart Wizard -->
                    
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      
                      <div id="step-1">
                        <form class="form-horizontal form-label-left">
                <div class="form-group">
                <label for="berat_badan">Berat Badan <span class="text-danger">*</span></label>
                <div class="input-group">
                  <input type="number" class="form-control" name="berat_badan" value="<?=set_value('berat_badan');?>">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Kg</div>
                  </div>
                </div>
                <small class="form-text text-muted">Berat badan pada satuan kilogram.</small>
                <?=form_error('berat_badan', '<small class="form-text text-danger">', '</small>');?>
              </div>
               <div class="form-group">
                <label for="tinggi_badan">Tinggi Badan <span class="text-danger">*</span></label>
                <div class="input-group">
                  <input type="number" class="form-control" name="tinggi_badan" value="<?=set_value('tinggi_badan');?>">
                  <div class="input-group-prepend">
                    <div class="input-group-text">CM</div>
                  </div>
                </div>
                <small class="form-text text-muted">Tinggi badan pada satuan sentimeter.</small>
                <?=form_error('tinggi_badan', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="jarak">Jarak Rumah Ke Sekolah <span class="text-danger">*</span></label>
                <div class="input-group">
                  <input type="number" class="form-control" name="jarak" value="<?=set_value('jarak');?>">
                  <div class="input-group-prepend">
                    <div class="input-group-text">KM</div>
                  </div>
                </div>
                <small class="form-text text-muted">Jarak rumah ke sekolah pada satuan kilo Meter.</small>
                <?=form_error('jarak', '<small class="form-text text-danger">', '</small>');?>
              </div>
               <div class="form-group">
                <label for="waktu">Waktu Tempuh  <span class="text-danger">*</span></label>
                <div class="input-group">
                  <input type="number" class="form-control" name="waktu" value="<?=set_value('waktu');?>">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Menit</div>
                  </div>
                </div>
                <small class="form-text text-muted">Waktu Tempuh rumah ke sekolah pada satuan sentimeter.</small>
                <?=form_error('waktu', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="anak">Anak ke: <span class="text-danger">*</span></label>
                <input type="number" class="form-control" name="anak" value="<?=set_value('anak');?>">
                <?=form_error('anak', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="jumlah_saudara">Jumlah Saudara Kandung <span class="text-danger">*</span></label>
                <input type="number" class="form-control" name="jumlah_saudara" value="<?=set_value('jumlah_saudara');?>">
                <small class="form-text text-muted">Jumlah saudara kandung yang dimiliki peserta didik.</small>
                <?=form_error('jumlah_saudara', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <button type="submit" name="tambah" class="btn btn-primary float-right">Kirim</button>

                        </form>

                      </div>
                     
                </div>
            </div>       
        </div>
    </div>



</html>