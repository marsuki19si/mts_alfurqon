<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Custom Theme Style -->
    <link href=<?= base_url("assets3/build/css/custom.min.css") ?> rel="stylesheet">
</head>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Formulir Pendaftaran </h3>
                <br>Simbol (<span class="text-danger">*</span>) Menandakan Wajib Diisi.</br>
            </div>
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 ">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Data Orang Tua</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <!-- Smart Wizard -->

                            <div id="wizard" class="form_wizard wizard_horizontal">

                                <div id="step-1">
                                    <form class="form-horizontal form-label-left">

                                        <div class="card-header">DATA AYAH KANDUNG</div>
            <div class="card-body">
              <div class="form-group">
                <label for="nama_ayah">Nama ayah Kandung <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="nama_ayah" value="<?=set_value('nama_ayah');?>">
                <small class="form-text text-muted">Nama ayah Kandung peserta didik sesuai dokumen resmi yang berlaku. Hindari penggunaan gelar akademik atau sosial (seperti Alm., Dr., Drs., S.Pd, dan H.)</small>
                <?=form_error('nama_ayah', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="tempat_lahir_ayah">Tempat Lahir Ayah <span class="text-danger">*</span></label>
                <input type="text" class="form-control datepicker" name="tempat_lahir_ayah" value="<?=set_value('tempat_lahir_ayah');?>">
                <small class="form-text text-muted">Tahun lahir ayah Kandung peserta didik.</small>
                <?=form_error('tempat_lahir_ayah', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="tanggal_lahir_ayah">Tanggal Lahir Ayah <span class="text-danger">*</span></label>
                <input type="date" class="form-control" name="tanggal_lahir_ayah" value="<?= set_value('tanggal_lahir_ayah'); ?>">
                <small class="form-text text-muted">Tanggal lahir Ayah sesuai dokumen resmi yang berlaku.</small>
                <?= form_error('tanggal_lahir_ayah', '<small class="form-text text-danger">', '</small>'); ?>
              </div>
               <div class="form-group">
                <label for="keterangan_ayah">Keterangan Ayah <span class="text-danger">*</span></label>
               <select class="form-control" id="exampleFormControlSelect1">
                <option>Masih Hidup</option>
                <option>Meninggal Dunia</option>
                </select>
                <small class="form-text text-muted">Keterangan ayah kadung peserta didik.</small>
                <?=form_error('keterangan_ayah', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="pekerjaan_ayah">Pekerjaan Ayah <span class="text-danger"></span></label>
                <select class="form-control" id="exampleFormControlSelect1">
                <option>Tidak Bekerja</option>
                <option>Nelayan</option>
                <option>Petani</option>
                <option>Peternak</option>
                <option>PNS/TNI/POLRI</option>
                <option>Karyawan Swasta</option>
                <option>Pedagang Kecil</option>
                <option>Pedagang Besar</option>
                <option>Pedagang Besar</option>
                <option>Wiraswasta</option>
                <option>Wirausaha</option>
                <option>Buruh</option>
                <option>Pensiunan</option>
                <option>Lain-lain</option>
                </select>
                <small class="form-text text-muted">Pekerjaan utama ayah kandung peserta didik. Pilih Meninggal Dunia apabila ayah Kandung peserta didik telah meninggal.</small>
                <?=form_error('pekerjaan_ayah', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="penghasilan_ayah">Penghasilan Bulanan <span class="text-danger"></span></label>
                 <select class="form-control" id="exampleFormControlSelect1">
                <option>Kurang dari 500.000</option>
                <option>500.000 - 999.999</option>
                <option>1 juta - 1.999.999</option>
                <option>2 juta - 4.999.999</option>
                <option>5 juta - 20 juta</option>
                <option>Lebih dari 20 juta</option>
                </select>
                <small class="form-text text-muted">Rentang penghasilan ayah kadung peserta didik. Kosongkan kolom ini apabila ayah kandung peserta didik telah meninggal dunia atau tidak bekerja.</small>
                <?=form_error('penghasilan_ayah', '<small class="form-text text-danger">', '</small>');?>
              </div>
            </div>

            <div class="card-header">DATA IBU KANDUNG</div>
            <div class="card-body">
              <div class="form-group">
                <label for="nama_ibu">Nama ibu Kandung <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="nama_ibu" value="<?=set_value('nama_ibu');?>">
                <small class="form-text text-muted">Nama ibu Kandung peserta didik sesuai dokumen resmi yang berlaku. Hindari penggunaan gelar akademik atau sosial (seperti Alm., Dr., Drs., S.Pd, dan H.)</small>
                <?=form_error('nama_ibu', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="tempat_lahir_ibu">Tempat Lahir Ibu <span class="text-danger">*</span></label>
                <input type="text" class="form-control datepicker" name="tempat_lahir_ibu" value="<?=set_value('tempat_lahir_ibu');?>">
                <small class="form-text text-muted">Tempat lahir ibu Kandung peserta didik.</small>
                <?=form_error('tempat_lahir_ibu', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="tanggal_lahir_ibu">Tanggal Lahir Ibu <span class="text-danger">*</span></label>
                <input type="date" class="form-control" name="tanggal_lahir_ibu" value="<?= set_value('tanggal_lahir_ibu'); ?>">
                <small class="form-text text-muted">Tanggal lahir Ayah sesuai dokumen resmi yang berlaku.</small>
                <?= form_error('tanggal_lahir_ibu', '<small class="form-text text-danger">', '</small>'); ?>
              </div>
               <div class="form-group">
                <label for="keterangan_ibu">Keterangan Ibu <span class="text-danger">*</span></label>
               <select class="form-control" id="exampleFormControlSelect1">
                <option>Masih Hidup</option>
                <option>Meninggal Dunia</option>
                </select>
                <small class="form-text text-muted">Keterangan ibu kadung peserta didik.</small>
                <?=form_error('keterangan_ibu', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="pekerjaan_ibu">Pekerjaan Ibu <span class="text-danger"></span></label>
                <select class="form-control" id="exampleFormControlSelect1">
                <option>Tidak Bekerja</option>
                <option>Nelayan</option>
                <option>Petani</option>
                <option>Peternak</option>
                <option>PNS/TNI/POLRI</option>
                <option>Karyawan Swasta</option>
                <option>Pedagang Kecil</option>
                <option>Pedagang Besar</option>
                <option>Pedagang Besar</option>
                <option>Wiraswasta</option>
                <option>Wirausaha</option>
                <option>Buruh</option>
                <option>Pensiunan</option>
                <option>Lain-lain</option>
                </select>
                <small class="form-text text-muted">Pekerjaan utama ibu kandung peserta didik. Pilih Meninggal Dunia apabila ayah Kandung peserta didik telah meninggal.</small>
                <?=form_error('pekerjaan_ibu', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="penghasilan_ibu">Penghasilan Bulanan <span class="text-danger"></span></label>
                <select class="form-control" id="exampleFormControlSelect1">
                <option>Kurang dari 500.000</option>
                <option>500.000 - 999.999</option>
                <option>1 juta - 1.999.999</option>
                <option>2 juta - 4.999.999</option>
                <option>5 juta - 20 juta</option>
                <option>Lebih dari 20 juta</option>
                </select>
                <small class="form-text text-muted">Rentang penghasilan ibu kadung peserta didik. Kosongkan kolom ini apabila ibu kandung peserta didik telah meninggal dunia atau tidak bekerja.</small>
                <?=form_error('penghasilan_ibu', '<small class="form-text text-danger">', '</small>');?>
              </div>
              <div class="form-group">
                <label for="alamat_ortu">Alamat Jalan <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="alamat_ortu" value="<?= set_value('alamat_ortu'); ?>">
                <small class="form-text text-muted">Jalur tempat tinggal peserta didik, terdiri atas gang, kompleks, blok, nomor rumah, dan sebagainya selain informasi yang diminta oleh kolom-kolom yang lain pada bgian ini. Sebagai contoh: peserta didik tinggal di sebuah kompleks perumahan Griya Adam yang berada pada Jalan Kemanggisan, dengan nomor rumah 4-C, di lingkungan RT 005 dan RW 011, Dusun Cempaka, Desa Salatiga. Maka dapat diisi dengan Jl. Kemanggisan, Komp. Griya Adam, No. 4-C.</small>
                <?= form_error('alamat_ortu', '<small class="form-text text-danger">', '</small>'); ?>
              </div>
            </div>
            <button type="submit" name="tambah" class="btn btn-primary float-right">Kirim</button>
                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>



</html>