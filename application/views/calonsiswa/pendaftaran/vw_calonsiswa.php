<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Custom Theme Style -->
  <link href=<?= base_url("assets3/build/css/custom.min.css") ?> rel="stylesheet">
</head>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Formulir Pendaftaran </h3>
        <br>Simbol (<span class="text-danger">*</span>) Menandakan Wajib Diisi.</br>
      </div>

      <div class="clearfix"></div>

      <div class="row">

        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Biodata Diri</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">


              <!-- Smart Wizard -->

              <div id="wizard" class="form_wizard wizard_horizontal">

                <div id="step-1">
                  <form action="<?= base_url('CalonSiswa/Pendaftaran/CalonSiswa/add'); ?>" method="POST"
                    class="form-horizontal form-label-left">

                    <div class="form-group">
                      <label for="jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span></label>
                      <select class="form-control" id="exampleFormControlSelect1" name="jenis_kelamin">
                        <option value="l">Laki-laki</option>
                        <option value="p">Perempuan</option>
                      </select>
                      <small class="form-text text-muted"></small>
                      <?= form_error('jenis_kelamin', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="nisn">NISN <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" name="nisn" value="<?= set_value('nisn'); ?>">
                      <small class="form-text text-muted">Nomor Induk Nasional peserta didik (jika memiliki). Jika belum
                        memiliki, maka wajib dikosongkan. NISN memiliki format 10 digit angka. contoh:
                        0009321234.</small>
                      <?= form_error('nisn', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="tempat_lahir">Tempat Lahir <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" name="tempat_lahir"
                        value="<?= set_value('tempat_lahir'); ?>">
                      <small class="form-text text-muted">Tempat lahir peserta didik sesuai dokumen resmi yang
                        berlaku.</small>
                      <?= form_error('tempat_lahir', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="tanggal_lahir">Tanggal Lahir <span class="text-danger">*</span></label>
                      <input type="date" class="form-control" name="tanggal_lahir"
                        value="<?= set_value('tanggal_lahir'); ?>">
                      <small class="form-text text-muted">Tanggal lahir peserta didik sesuai dokumen resmi yang
                        berlaku.</small>
                      <?= form_error('tanggal_lahir', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat Jalan <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" name="alamat" value="<?= set_value('alamat'); ?>">
                      <small class="form-text text-muted">Jalur tempat tinggal peserta didik, terdiri atas gang,
                        kompleks, blok, nomor rumah, dan sebagainya selain informasi yang diminta oleh kolom-kolom yang
                        lain pada bgian ini. Sebagai contoh: peserta didik tinggal di sebuah kompleks perumahan Griya
                        Adam yang berada pada Jalan Kemanggisan, dengan nomor rumah 4-C, di lingkungan RT 005 dan RW
                        011, Dusun Cempaka, Desa Salatiga. Maka dapat diisi dengan Jl. Kemanggisan, Komp. Griya Adam,
                        No. 4-C.</small>
                      <?= form_error('alamat', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="tempat_tinggal">Tempat Tinggal <span class="text-danger">*</span></label>
                      <select class="form-control" id="exampleFormControlSelect1" name="tempat_tinggal">
                        <option value="Orang Tua">Bersama Orang Tua</option>
                        <option value="Wali">Wali</option>
                        <option value="Kos">Kos</option>
                        <option value="Panti Asuhan">Panti Asuhan</option>
                        <option value="Asrama">Asrama</option>
                        <option value="Lainnya">Lainnya</option>
                      </select>
                      <small class="form-text text-muted">Kepemilikan tempat tinggal peserta didik saat ini (yang telah
                        diisikan pada kolom-kolom sebelumnya di atas).</small>
                      <?= form_error('tempat_tinggal', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                      <label for="transportasi">Moda Transportasi <span class="text-danger">*</span></label>
                      <select class="form-control" id="exampleFormControlSelect1" name="transportasi">
                        <option value="Jalan Kaki">Jalan Kaki</option>
                        <option value="Kendaraan Pribadi">Kendaraan Pribadi</option>
                        <option value="Ojek">Ojek</option>
                      </select>
                      <small class="form-text text-muted">Jenis transportasi utama atau yang paling sering digunakan
                        peserta didik untuk berangkat kesekolah</small>
                      <?= form_error('transportasi', '<small class="form-text text-danger">', '</small>'); ?>
                    </div>

                    <button type="submit" name="tambah" class="btn btn-primary float-right">Kirim</button>
                  </form>

                </div>

              </div>
            </div>
          </div>
        </div>



</html>