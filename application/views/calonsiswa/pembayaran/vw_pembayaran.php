<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Custom Theme Style -->
    <link href=<?= base_url("assets3/build/css/custom.min.css") ?> rel="stylesheet">
</head>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Pembayaran </h3>
               
            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 ">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Bukti Pembayaran</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <!-- Smart Wizard -->

                            <div id="wizard" class="form_wizard wizard_horizontal">

                                <div id="step-1">
                                    <form class="form-horizontal form-label-left">

                                        <div class="form-group">
                                            <label for="nominal">Nominal Pembayaran <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="nominal"
                                                value="<?= set_value('nominal'); ?>">
                                            <small class="form-text text-muted"></small>
                                        </div>
                                        <div class="form-group">
                <label for="gambar">Bukti Bayar <span class="text-danger">*</span></label>
                <input class="form-control" type="file" name="gambar">
                <small class="form-text text-muted">Foto scan bukti pembayaran. ukuran max 3MB. , format: JPG,JPEG,PNG</small>
                <?=form_error('gambar', '<small class="form-text text-danger">', '</small>');?>
              </div>
                                        <button type="submit" name="tambah"
                                            class="btn btn-primary float-right">Kirim</button>
                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>



</html>